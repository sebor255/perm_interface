from dataclasses import dataclass

@dataclass
class FerromagneticCore():
    diameter: float = None
    notes: str = None
    upProbes = []
    downProbes = []

    def add_probe_up(self, probe: tuple):
        self._add_probe(probe, isDown=False)

    def add_probe_down(self, probe: tuple):
        self._add_probe(probe)

    def _add_probe(self, probe: tuple, isDown=True):
        # probe (value, time)
        if isDown == True:
            self.downProbes.append(probe)
        else:
            self.upProbes.append(probe)


