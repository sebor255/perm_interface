import json
import csv
import datetime
from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QIntValidator
from PyQt5.QtWidgets import QMainWindow, QHBoxLayout, QFileDialog
from matplotlib.pyplot import connect

from design.designMainWindow import Ui_MainWindow
from graph import Graph
from httpConn import HTTPConn
from meaurement import FerromagneticCore
from pathlib import Path

class MainWindow(QMainWindow):

## general ##
    def __init__(self, parent=None) -> None:
        super().__init__(parent)
        self.ferroCores: list(FerromagneticCore) = []
        self.isMeasureStarted = False
        self.currentFerroCore = None
        self.httpConn = HTTPConn()
        self.httpTimer = QTimer()
        self.httpTimerTimeout = 333
        self.outputPath = Path("./outputs")
        self.setupUI()
        self.connectItems()
        self.printMsg("Enter IpAddress and press connect")
        

    def setupUI(self):
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.graph_layout = QHBoxLayout(self.ui.groupBox_graph)
        self.graph_down = Graph()
        self.graph_up = Graph()
        self.graph_layout.addWidget(self.graph_down)
        self.graph_layout.addWidget(self.graph_up)
        self.ui.lineEdit_diameter.setValidator(QIntValidator())
        
    def connectItems(self):
        self.httpTimer.timeout.connect(self.httpTimer_expired)
        self.ui.pushButton_connect.clicked.connect(self.pushButton_connect_action)
        self.ui.lineEdit_ipAddress.textEdited.connect(self.lineEdit_ipAddress_action)
        self.ui.pushButton_startMeasure.clicked.connect(self.pushButton_startMeasure_action)
        self.ui.actionSelect_output.triggered.connect(self.tab_selectOutputPath)
        self.ui.lineEdit_diameter.textEdited.connect(self.lineEdit_diameter_action)

## elements ##
    def lineEdit_diameter_action(self):
        self.currentFerroCore = FerromagneticCore()
        self.currentFerroCore.diameter = self.ui.lineEdit_diameter.text()

    def pushButton_connect_action(self):
        if self.httpConn.isConnected == False:
            self.printMsg("connecting...")
            if self.httpConn.connect():
                self.printMsg("connected")  
                self.httpTimer.start(self.httpTimerTimeout)
                self.ui.pushButton_startMeasure.setEnabled(True)
                self.ui.pushButton_stopMeasure.setEnabled(True)
            else: self.printMsg("not connected")
        else:
            self.httpConn.disconnect()
            self.httpTimer.stop()
            self.ui.pushButton_startMeasure.setEnabled(False)
            self.ui.pushButton_stopMeasure.setEnabled(False)
            self.printMsg("disconnected")

    def pushButton_startMeasure_action(self):
        if not self.currentFerroCore.diameter:
            self.printMsg("Please provide diameter of the ferrite core")
            return
        body = self.httpConn.getBodyFromReq("/startMeasurement")
        if "Status: started" in body:
            self.printMsg("measurement started")
            self.graph_layout.removeWidget(self.graph_up)
            self.graph_layout.removeWidget(self.graph_down)
            self.graph_down = Graph()
            self.graph_up = Graph()
            self.graph_layout.addWidget(self.graph_down)
            self.graph_layout.addWidget(self.graph_up)
            self.isMeasureStarted = True
            self.httpTimer.start()

    def lineEdit_ipAddress_action(self):
        text = self.ui.lineEdit_ipAddress.text()
        self.httpConn.ipAddress = text
        self.printMsg(text)
        
    def printTextBrowser(self, msg):
        self.ui.textBrowser_measure.append(msg)

    def printMsg(self, msg):
        self.statusBar().showMessage("Status: " + msg)

    def tab_selectOutputPath(self):
        self.outputPath = Path(QFileDialog.getExistingDirectory(self, "Select Directory"))


## timers ##
    def httpTimer_expired(self):
        if self.isMeasureStarted:
            body = self.httpConn.getBodyFromReq("/measure")
            try:
                body = json.loads(body)
            except:
                return
            value = body["PermValue"]
            probeTime = body["ProbeTime"]
            direction = body["Direction"]
            status = body["Status"]
            if "measuring" not in status:
                self.httpTimer.stop()
                self.measurementEnd_action()
                return 
            self.printTextBrowser(value)
            if direction == "Down":
                self.currentFerroCore.add_probe_down((value,probeTime))
                self.graph_down.addProbe((float(value),float(probeTime)))
            elif direction == "Up":
                self.currentFerroCore.add_probe_up((value,probeTime))
                self.graph_up.addProbe((float(value),float(probeTime)))
            
## rest ##
    def measurementEnd_action(self):
        if self.ui.actionSave_pictures.isEnabled():
            self.graph_up.saveImage(self.outputPath, f"{self.currentFerroCore.diameter}_up")
            self.graph_down.saveImage(self.outputPath, f"{self.currentFerroCore.diameter}_down")
        if self.ui.actionSave_csv.isEnabled():
            self.saveToCsv(self.outputPath, f"{self.currentFerroCore.diameter}_up", self.currentFerroCore.upProbes)
            self.saveToCsv(self.outputPath, f"{self.currentFerroCore.diameter}_down", self.currentFerroCore.downProbes)
    
    def saveToCsv(self, path, filename, iterable):
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        fullpath = path / f"{filename}_{timestr}.csv"
        with open(fullpath, 'w') as f:
            writer = csv.writer(f)
            writer.writerows(iterable)