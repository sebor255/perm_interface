# Only needed for access to command line arguments
import sys

from PyQt5.QtWidgets import QApplication
from mainWindow import MainWindow


if __name__ == "__main__":
    # One QApplication instance per application.
    # Pass in sys.argv to allow command line arguments for your app.
    # If you know you won't use command line arguments QApplication([]) works too.
    app = QApplication(sys.argv)
    # Create a Qt main window, which will be our window.
    mainWindow = MainWindow()
    mainWindow.show()
    # Start the event loop.
    app.exec()