import datetime
import pyqtgraph
import pyqtgraph.exporters
from pathlib import Path
import datetime
import os




class Graph(pyqtgraph.PlotWidget):
    def __init__(self, parent=None, background='default', plotItem=None, **kargs):
        super().__init__(parent, background, plotItem, **kargs)
        self.values = []
        self.times = []
        self.exporter = None

    def addProbe(self, probe):
        self.values.append(probe[0])
        self.times.append(probe[1])
        self.plot(self.times, self.values)

    def saveImage(self, filepath: Path, name=None):
        self.exporter = pyqtgraph.exporters.ImageExporter(self.plotItem)
        self.exporter.parameters()['width'] = 500
        timestr = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        if not os.path.exists(filepath):
            os.makedirs(filepath)
        if name:
            outputpath = filepath / f"{name}_{timestr}.png"
        else:
            outputpath = filepath / f"{timestr}.png"

        self.exporter.export(str(outputpath.absolute()))