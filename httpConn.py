import _thread
from dataclasses import dataclass
import http.client

@dataclass
class HTTPConn:
    conn: http.client.HTTPConnection = None
    ipAddress: str = "None"
    port: int = 80
    isConnected: bool = False

    def connect(self) -> bool:
        self._connect()
        return self.isConnected

    def disconnect(self) -> bool:
        self._disconnect()
        return self.isConnected

    def _connect(self):
        try:
            if self.ipAddress == "None": return False
            self.conn = http.client.HTTPConnection(self.ipAddress, 80)
            self.conn.request("GET", "/")
            resp = self.conn.getresponse()
            resp.read(200)
            self.isConnected = True if resp.status == 200 else False
        except Exception as e:
            print(e)
            self.isConnected = False

    def _disconnect(self):
        self.conn = None
        self.isConnected = False

    def request(self, req: str) -> str:
        try:
            self.conn.request("GET", req)
            resp = self.conn.getresponse()
            return resp.read(300)
        except Exception as e:
            print(e)

    def getBodyFromReq(self, req: str) -> str:
        resp = self.request(req)
        start = resp.decode().find("<body>") + len("<body>")
        stop = resp.decode().find("</body>")
        return resp[start:stop].decode()
